<?php if ($this->session->userdata('status') == 'login') {
  redirect(base_url('products'));
} ?>
<!doctype html>
<html>
<head>
  <?php $this->load->view('_partials/head'); ?>
</head>
<body id="LoginForm">
  <div class="container">
    <h1 class="form-heading">Register Form</h1>
    <div class="login-form">
      <div class="main-div" >
        <div class="panel">
         <h2>User Register</h2>
       </div>
       <form id="Login" action="<?= base_url('register') ?>" method="post">
        <div class="form-group">

          <input type="text" class="form-control" name="username" id="username" placeholder="Username Anda">

        </div>
        <div class="form-group">

          <input type="password" class="form-control" name="password" id="password" placeholder="Password Anda">

        </div>
        <div class="form-group">

          <input type="text" class="form-control" name="address" id="address" placeholder="Alamat Anda">

        </div>
        <div class="form-group">

          <input type="email" class="form-control" name="email" id="email" placeholder="Email Anda">

        </div>
        <button type="submit" name="submit" class="btn btn-primary">Register</button>
        <div class="forgot">
          <center>Already Have An Account? <a href="<?= base_url() ?>">Login</a>
          </center>
        </div>
      </form>
    </div>
  </div>


</body>

<?php $this->load->view('_partials/js.php'); ?>

</html>
