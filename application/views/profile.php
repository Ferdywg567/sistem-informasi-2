<!doctype html>
<html lang="en">
<head>
	<?php $this->load->view('_partials/head.php'); ?>
</head>
<body>

    <div class="wrapper">
        <div class="sidebar" data-color="red" data-image="<?= base_url('img/sidebar-5.jpg') ?>">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="" class="simple-text">
                Toko Makmur Jaya
            </a>
        </div>

        <ul class="nav">
            <li class="active">
                <a href="<?= base_url('products') ?>">
                    <i class="pe-7s-note2"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="<?= base_url('profile') ?>">
                    <i class="pe-7s-user"></i>
                    <p>User Profile</p>
                </a>
            </li>
            <li>
                <a href="<?= base_url('add') ?>">
                    <i class="pe-7s-note"></i>
                    <p>Add Product</p>
                </a>
            </li>
            <li>
                <a href="<?= base_url('history') ?>">
                  <i class="pe-7s-cart"></i>
                  <p>Transaction History</p>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Profile</a>
            </div>
            <div class="collapse navbar-collapse">

                <ul class="nav navbar-nav navbar-right">
                   <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span>
                            <i class="far fa-user"></i>
                            <?= $this->session->userdata('username') ?>
                            <b class="caret"></b>
                        </span>

                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?= base_url('profile') ?>">Account</a></li>
                        <li class="divider"></li>
                        <li><a href="<?= base_url('logout') ?>">Log Out</a></li>
                    </ul>
                </li>
                <li class="separator hidden-lg"></li>
            </ul>
        </div>
    </div>
</nav>


  <div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Edit Profile</h4>
                    </div>
                    <div class="content">
                        <form>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="card card-user">
                                        <div class="image">
                                            <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                                        </div>
                                        <div class="content">
                                            <div class="author">
                                             <a href="#">
                                                <img class="avatar border-gray" src="<?= base_url('img/faces/face-3.jpg') ?>" alt="..."/>

                                                <h4 class="title">Ferdy Rohim<br />
                                                 <small>ferdywg567</small>
                                             </h4>
                                         </a>
                                     </div>
                                     <p class="description text-center"> "Hello, <br>
                                        My name is Ferdyansyah Nur Rohim <br>
                                        you can call me Ferdy"
                                    </p>    
                                </div>
                                <hr>
                                <div class="text-center">
                                    <button href="#" class="btn btn-simple"><i class="fab fa-facebook-square"></i></button>
                                    <button href="#" class="btn btn-simple"><i class="fab fa-twitter"></i></button>
                                    <button href="#" class="btn btn-simple"><i class="fab fa-google-plus-square"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label>Company (disabled)</label>
                                <input type="text" class="form-control" disabled placeholder="Company" value="Creative Code Inc.">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" placeholder="Username" value="ferdywg567">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input type="email" class="form-control" placeholder="Email">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name</label>
                                <input type="text" class="form-control" placeholder="Company" value="Ferdy">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name</label>
                                <input type="text" class="form-control" placeholder="Last Name" value="Rohim">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Home Address" value="Jl. Ngagel Mulyo Gg. 4 No.5">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="City" value="Surabaya">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Country</label>
                                <input type="text" class="form-control" placeholder="Country" value="Indonesia">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Postal Code</label>
                                <input type="number" class="form-control" placeholder="ZIP Code">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>About Me</label>
                                <textarea rows="5" class="form-control" placeholder="Here can be your description" value="Ferdy">Hello, My name is Ferdyansyah Nur Rohim, you can call me Ferdy</textarea>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info btn-fill pull-right">Update Profile</button>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
    </div>


</div>
</div>
</div>


<?php $this->load->view('_partials/footer.php'); ?>

</div>
</div>


</body>

<?php $this->load->view('_partials/js.php'); ?>

</html>
