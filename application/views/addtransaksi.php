<!doctype html>
<html lang="en">
<head>
	<?php $this->load->view('_partials/head.php'); ?>
  <script>
    var x = 0;
    var y = 0;
    var z = 0;

    function calc (obj) {
      var e = obj.id.toString();
      if (e == 'price') {
        x = Number(obj.value);
        y = Number(document.getElementById('stok').value);
      } else {
        x = Number(document.getElementById('price').value);
        y = Number(obj.value);
      }

      z = x * y;
      document.getElementById('total').value = z;
    }
  </script>

</head>
<body>

  <div class="wrapper">
    <div class="sidebar" data-color="orange" data-image="<?= base_url('img/sidebar-5.jpg') ?>">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

      -->

      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="" class="simple-text">
            Toko Makmur Jaya
          </a>
        </div>

        <ul class="nav">
          <li class="active">
            <a href="<?= base_url('products') ?>">
              <i class="pe-7s-note2"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="<?= base_url('profile') ?>">
              <i class="pe-7s-user"></i>
              <p>User Profile</p>
            </a>
          </li>
          <li>
            <a href="<?= base_url('add') ?>">
              <i class="pe-7s-note"></i>
              <p>Add Product</p>
            </a>
          </li>
          <li>
            <a href="<?= base_url('history') ?>">
              <i class="pe-7s-cart"></i>
              <p>Transaction History</p>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">
      <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Add Transacation</a>
          </div>
          <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span>
                  <i class="far fa-user"></i>
                  <?= $this->session->userdata('username') ?>
                  <b class="caret"></b>
                </span>

              </a>
              <ul class="dropdown-menu">
                <li><a href="<?= base_url('profile') ?>">Account</a></li>
                <li class="divider"></li>
                <li><a href="<?= base_url('logout') ?>">Log Out</a></li>
              </ul>
            </li>
            <li class="separator hidden-lg"></li>
          </ul>
        </div>
      </div>
    </nav>


    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card">

              <div class="header">
                <h4 class="title">Transaksi</h4>
                <p class="category">isikan Form dibawah ini untuk membeli barang</p>
              </div>
              <div class="content">
                <div class="container-fluid">
                 <form class="form-horizontal" action="#" method="post">
                  <div class="form-group">
                    <div class="col-sm-10">
                      <input type="hidden" name="id_product" class="form-control" value="<?= $product->id_product ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="name"><b>Nama Barang:</b></label>
                    <div class="col-sm-10">
                      <input type="text" name="name" class="form-control <?= form_error('name') ? 'is Invalid':'' ?>" id="name" placeholder="Nama Barang" value="<?= $product->name ?>" readonly>
                      <div class="invalid-feedback text-danger">
                        <?= form_error('name') ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="price"><b>Harga Barang:</b></label>
                    <div class="col-sm-10">
                      <input type="number" name="price" class="form-control <?= form_error('price') ? 'is Invalid':'' ?>" id="price" placeholder="Harga Barang" value="<?= $product->price ?>" readonly>
                      <div class="invalid-feedback text-danger">
                        <?= form_error('price') ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="stok1"><b>Jumlah Beli:</b></label>
                    <div class="col-sm-10">
                      <input type="number" name="stok" class="form-control <?= form_error('stok') ? 'is Invalid':'' ?>" id="stok" placeholder="1" value="" onkeyup="calc(this)">
                      <div class="invalid-feedback text-danger">
                        <?= form_error('stok') ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-sm-2" for="total"><b>Total Barang:</b></label>
                    <div class="col-sm-10">
                      <input type="number" name="total" class="form-control <?= form_error('total') ? 'is Invalid':'' ?>" id="total" placeholder="1" readonly>
                      <div class="invalid-feedback text-danger">
                        <?= form_error('total') ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group pull-right">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" name="submit" class="btn btn-primary btn-fill">Submit</button>
                    </div>
                  </div>
                </form> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
    <?php $this->load->view('_partials/footer.php'); ?>
</div>
</div>


</body>

<?php $this->load->view('_partials/js.php'); ?>

<script type="text/javascript">
 $(document).ready(function(){

   demo.initChartist();

   $.notify({
     icon: 'pe-7s-gift',
     message: "Welcome to Toko Makmur Jaya - a beautiful freebie for every web developer."

   },{
    type: 'success',
    timer: 4000
  });

 });
</script>

</html>
