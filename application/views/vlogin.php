<?php if ($this->session->userdata('status') == 'login') {
  redirect(base_url('products'));
} ?>
<!doctype html>
<html>
<head>
  <?php $this->load->view('_partials/head'); ?>
</head>
<body id="LoginForm">
  <div class="container">
    <h1 class="form-heading">login Form</h1>
    <div class="login-form">
      <div class="main-div">
        <div class="panel">
         <h2>Admin Login</h2>
         <p>Please enter your Name and password</p>
       </div>
       <form id="Login" action="<?= base_url('aksi') ?>" method="post">

        <div class="form-group">

          <input type="text" class="form-control" name="username" id="username" placeholder="Username Anda">

        </div>

        <div class="form-group">

          <input type="password" class="form-control" name="password" id="password" placeholder="Password Anda">

        </div>
        <div class="forgot">
          <a href="reset.html">Forgot password?</a>
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Login</button>
        <div class="forgot">
          <center>Doesn't Have An Account Yet? <a href="<?= base_url('register') ?>">Register</a>
          </center>
        </div>

      </form>
    </div>
  </div>
</div>


</body>

<?php $this->load->view('_partials/js.php'); ?>

</html>
