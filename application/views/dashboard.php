<!doctype html>
<html lang="en">
<head>
	<?php $this->load->view('_partials/head.php'); ?>

</head>
<body>

  <div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="<?= base_url('img/sidebar-5.jpg') ?>">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

      -->

      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="" class="simple-text">
            Toko Makmur Jaya
          </a>
        </div>

        <ul class="nav">
          <li class="active">
            <a href="<?= base_url('products') ?>">
              <i class="pe-7s-note2"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li>
            <a href="<?= base_url('profile') ?>">
              <i class="pe-7s-user"></i>
              <p>User Profile</p>
            </a>
          </li>
          <li>
            <a href="<?= base_url('add') ?>">
              <i class="pe-7s-note"></i>
              <p>Add Product</p>
            </a>
          </li>
          <li>
            <a href="<?= base_url('history') ?>">
              <i class="pe-7s-cart"></i>
              <p>Transaction History</p>
            </a>
          </li>
        </ul>
      </div>
    </div>

    <div class="main-panel">
      <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Dashboard</a>
          </div>
          <div class="collapse navbar-collapse">

            <ul class="nav navbar-nav navbar-right">
             <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <span>
                  <i class="far fa-user"></i>
                  <?= $this->session->userdata('username') ?>
                  <b class="caret"></b>
                </span>

              </a>
              <ul class="dropdown-menu">
                <li><a href="<?= base_url('profile') ?>">Account</a></li>
                <li class="divider"></li>
                <li><a href="<?= base_url('logout') ?>">Log Out</a></li>
              </ul>
            </li>
            <li class="separator hidden-lg"></li>
          </ul>
        </div>
      </div>
    </nav>


    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col">
            <div class="card">

              <div class="header">
                <h4 class="title">Table Barang</h4>
                <p class="category">List Barang in Database</p>
              </div>

              <div class="content table-responsive table-full-width">
                <table class="table table-hover">
                  <thead>
                    <th>
                      <center>Id Barang</center>
                    </th>
                    <th>
                      <center>Nama Barang</center>
                    </th>
                    <th>
                      <center>Harga Barang</center>
                    </th>
                    <th>
                      <center>Jumlah Barang</center>
                    </th>
                    <th>
                      <center>Action</center>
                    </th>
                  </thead>
                  <tbody>
                    <?php foreach ($products as $key => $product): ?>
                      <tr>
                       <td>
                        <center><?= $key+1 ?></center>
                      </td>
                      <td>
                       <center><?=  $product->name ?></center>
                     </td>
                     <td>
                       <center><?= 'Rp.'.$product->price.',-' ?></center>
                     </td>
                     <td>
                       <center><?= $product->stok ?></center>
                     </td>
                     <td>
                       <center>
                         <a class="btn btn-success" href="<?= base_url('products/edit/'.$product->id_product) ?>">
                          Edit
                        </a>
                        <a onclick="deleteConfirm('<?= base_url('products/delete/'.$product->id_product) ?>')" href="#!" data-target="#deleteModal" data-toggle="modal">
                          <button class="btn btn-danger">
                            Delete
                          </button>
                        </a>
                        <a class="btn btn-warning" href="<?= base_url('products/transaksi/'.$product->id_product) ?>">
                          Transaksi
                        </a>
                      </center>
                    </td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('_partials/footer.php'); ?>

</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
      <div class="modal-footer">
        <button class="btn btn-info" type="button" data-dismiss="modal">Cancel</button>
        <a id="btn-delete" class="btn btn-danger" href="#">Delete</a>
      </div>
    </div>
  </div>
</div>

</body>

<?php $this->load->view('_partials/js.php'); ?>

<script type="text/javascript">
 $(document).ready(function(){

   demo.initChartist();

   $.notify({
     icon: 'pe-7s-gift',
     message: "Welcome to Toko Makmur Jaya - a beautiful freebie for every web developer."

   },{
    type: 'info',
    timer: 4000
  });

 });
 function deleteConfirm (url) {
  $('#btn-delete').attr('href', url);
}
</script>

</html>
