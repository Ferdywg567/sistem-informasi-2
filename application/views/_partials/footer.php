<footer class="footer">
    <div class="container-fluid">
        <p class="copyright">
            &copy; <script>document.write(new Date().getFullYear())</script> <a href="#">Toko Makmur Jaya</a>, made with love for a better web
        </p>
    </div>
</footer>