<meta charset="utf-8" />
<link rel="icon" type="image/png" href="<?= base_url('img/favicon.ico') ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<title>Light Bootstrap Dashboard by Creative Tim</title>

<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
<meta name="viewport" content="width=device-width" />


<!-- Bootstrap core CSS     -->
<link href="<?= base_url('css/bootstrap.min.css') ?>" rel="stylesheet" />

<!-- Animation library for notifications   -->
<link href="<?= base_url('css/animate.min.css') ?>" rel="stylesheet"/>

<!--  Light Bootstrap Table core CSS    -->
<link href="<?= base_url('css/light-bootstrap-dashboard.css?v=1.4.0') ?>" rel="stylesheet"/>


<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="<?= base_url('css/demo.css') ?>" rel="stylesheet" />
<link href="<?= base_url('css/test.css') ?>" rel="stylesheet" />


<!--     Fonts and icons     -->
<link href="<?= base_url('css/all.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('css/pe-icon-7-stroke.css') ?>" rel="stylesheet" />