<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('muser');
		$this->load->library('session');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->load->view('vlogin');
	}


	public function aksi_login()
	{
		$post = $this->input->post();
		$username = $post['username'];
		$password = $post['password'];
		$where = [
		    'username' => $username, 
		    'password' => $password, 
		];

		$cek = $this->muser->cek_login('user',$where)->num_rows();

		if ($cek > 0) {

			$data_session = array(
				'username' => $username,
				'status' => 'login',
			);
			
			$this->session->set_userdata( $data_session );
			redirect(base_url('products'));
		}
		else {
			echo 'Username dan Password Anda Salah!!!';
		}
	}

	public function register()
	{
		$register = $this->muser;

		$validation = $this->form_validation;
		$validation->set_rules($this->muser->rules());

		if ($validation->run()) {

			$register->register();
			redirect(base_url());
		}
		$this->load->view('vregister');

	}
}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */