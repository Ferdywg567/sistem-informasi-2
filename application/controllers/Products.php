<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mproduct');
		$this->load->model('mtransaksi');
		$this->load->library('form_validation');

		if($this->session->userdata('status') != "login"){
			redirect(base_url());
		}
	}

	public function err()
	{
		echo '<center>Hayo.... Mau Ngapain :)</center>';
	}

	public function index()
	{
		$data['products'] = $this->mproduct->getAll();
		$this->load->view('dashboard', $data);		
	}

	public function profile()
	{
		$this->load->view('profile');
	}

	public function add()
	{
		$product = $this->mproduct;
		$validation = $this->form_validation;
		$validation->set_rules($product->rules());

		if ($validation->run()) {
			$product->save();
			$this->session->flashdata('success', 'Berhasil Disimpan');
			redirect(base_url('products'));
		}
		else {
			$this->load->view('addbarang');
		}
	}

	public function edit($id = null)
	{
		if (!isset($id)) {
			redirect(base_url());
		}

		$product = $this->mproduct;
		$validation = $this->form_validation;
		$validation->set_rules($product->rules());
		if ($validation->run()) {
			$product->update();
			redirect(base_url('products'));
		}		
		$data['product'] = $product->getById($id);
		if (!$data['product']) {
			redirect(base_url('products'));
		}
		$this->load->view('editbarang', $data);

	}

	public function delete($id = null)
	{
		if (!isset($id)) {
			show_404();
		}

		if ($this->mproduct->delete($id)) {
			redirect(base_url('products'));
		}
	}

	public function login()
	{
		$this->load->view('vlogin');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function transaksi($id = null)
	{
		if (!isset($id)) {
			redirect(base_url('products'));

		}
		$product = $this->mproduct;
		$transaksi = $this->mtransaksi;
		$validation = $this->form_validation;
		$validation->set_rules($transaksi->rules());

		if ($validation->run()) {
			$transaksi->save();
			redirect(base_url('products'));
		}
		
		$data['product'] = $product->getById($id);
		
		if (!$data['product']) {
			redirect(base_url('products'));
		}

		$this->load->view('addtransaksi', $data);
	}

	public function dataTransaksi()
	{
		$data['transaksi'] = $this->mtransaksi->getAll();
		$this->load->view('datatransaksi', $data);
	}

	public function deleteTransaksi($id = null)
	{
		if (!isset($id)) {
			show_404();
		}

		if ($this->mtransaksi->delete($id)) {
			redirect(base_url('transaksi'));
		}
	}

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */