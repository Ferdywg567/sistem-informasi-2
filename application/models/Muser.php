<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Muser extends CI_Model {

	private $table = 'user';
	public $username;
	public $password;
	public $address;
	public $email;
	public $about;

	public function rules()
	{
		return [
			[
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'required',
			],
			[
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required',
			],
			[
				'field' => 'address',
				'label' => 'Address',
				'rules' => 'required',
			],
			[
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required',
			],
		];
	}

	public function register()
	{
		$post = $this->input->post();
		$this->username = $post['username'];
		$this->password = $post['password'];
		$this->address = $post['address'];
		$this->email = $post['email'];

		$this->db->insert($this->table, $this);
	}

	public function get_user()
	{
		$post = $this->input->post();
		$id_user = $post['id_user'];
		$username = $post['username'];
		$password = $post['password'];
		$address = $post['address'];
		$email = $post['email'];
		$about = $post['about'];

	}

	public function cek_login($table,$where)
	{
		return $this->db->get_where($table, $where);
	}

}


/* End of file Mlogin.php */
/* Location: ./application/models/Mlogin.php */