<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransaksi extends CI_Model {

	private $table = 'transaksi';
	public $id_transaksi;
	public $id_product;
	public $name;
	public $price;
	public $stok;
	public $total;

	public function rules()
	{
		return [
			[
				'field' => 'stok',
				'label' => 'Stok',
				'rules' => 'required'
			]
		];
	}

	public function getAll()
	{
		return $this->db->get($this->table)->result();
	}

	public function getById($id)
	{
		return $this->db->get($this->table, ['id_transaksi' => $id])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this->id_product = $post['id_product'];
		$this->name = $post['name'];
		$this->price = $post['price'];
		$this->stok = $post['stok'];
		$this->total = $post['total'];
		$this->db->insert($this->table, $this);
	}

	public function delete($id)
	{
		return $this->db->delete($this->table, array('id_transaksi' => $id));
	}

}

/* End of file Mtransaksi.php */
/* Location: ./application/models/Mtransaksi.php */