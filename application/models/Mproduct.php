<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproduct extends CI_Model {

	private $table = 'products';
	public $id_product;
	public $name;
	public $price;
	public $stok;

	public function rules()
	{
		return [
			[
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'required',
			],
			[
				'field' => 'price',
				'label' => 'Price',
				'rules' => 'required',
			],
			[
				'field' => 'stok',
				'label' => 'Stok',
				'rules' => 'required',
			],
		];			
	}

	public function getAll()
	{
		return $this->db->get($this->table)->result();
	}

	public function getById($id)
	{
		return $this->db->get_where($this->table,['id_product' => $id])->row();
	}

	public function save()
	{
		$post = $this->input->post();
		$this->name = $post['name'];
		$this->price = $post['price'];
		$this->stok = $post['stok'];
		$this->db->insert($this->table, $this);
	}

	public function update()
	{
		$post = $this->input->post();
		$this->id_product = $post['id_product'];
		$this->name = $post['name'];
		$this->price = $post['price'];
		$this->stok = $post['stok'];
		$this->db->update($this->table, $this, array('id_product' => $this->id_product));

	}

	public function delete($id)
	{
		return $this->db->delete($this->table, array('id_product' => $id));
	}
}

/* End of file Mproduct.php */
/* Location: ./application/models/Mproduct.php */